+++
title = "Arch Setup Guide"
description = "A simple guide for getting Arch running on ASUS laptops"
sort_by = "none"
template = "page/wiki.html"
author = "Mateusz Schyboll"
+++

# Instaling

To install Arch on all ASUS laptops just follow the regular [installation guide](https://wiki.archlinux.org/title/installation_guide)

Only issue which might appear is nouveau crashing the instalation, this can be solved by adding a boot parameter `modprobe.blacklist=nouveau` to the kernel cmdline before booting the instalation media.
To edit the instalation media boot entry just press e on it and then put the blacklist parameter at the end off all parameters. Example:

![Blacklist nouveau](/images/guide_arch_blacklist_nouveau.png)

The same parameter can be used to boot the system after instaling as long you don't install nvidia drivers.

# Asusctl and custom kernel

The recommended way to install asusctl is using g14 pacman repo (don't get confused by the name, it exists only for historical reasons, it applies to all ASUS ROG laptops)
To get the repo add to your `/etc/pacman.conf` at the end:

```
[g14]
SigLevel = DatabaseNever Optional TrustAll
Server = https://arch.asus-linux.org
```

Then just install asusctl with 
```
pacman -Sy asusctl
```

## Custom kernel

Newer devices often require custom kernel with patches, that kernel is also available in the g14 pacman repo, to install it just run:
```
pacman -Sy linux-g14 linux-g14-headers
```
Again, don't get confused by the name, it exists only for historical reasons.
If you are using kernel use the `nvidia-dkms` package for nvidia drivers, the regular `nvidia` package works only with stock Arch kernel
2020 models should work fine with stock Arch kernel, 2021 currently requires that kernel gets the latest fixes.

There is also a xanmod kernel with all the patches needed [xanmod-rog](https://github.com/arglebargle-arch/xanmod-rog-PKGBUILD) which might give you better perfomence in games.

After instaling the new kernel you need to regenerate your boot menu or add a new boot entry depending on what boot manager you are using. For GRUB that will be:
```
grub-mkconfig -o /boot/grub/grub.cfg
```
For others refer to their documentation/Arch Wiki page. You can check currently booted kernel with command `uname -r`. It should give you for example:
```
5.12.10-arch1-g14-1
```
The `-g14` part is the important one.

# Other distros based on Arch

To ease the installation you can use other distro based on Arch, only Manjaro is highly not recommend (it is not really based on Arch and might not be compatible with points above and things might break with upgrades).

Recommended ones are: RebornOS and EndeavourOS.

Blacklisting nouveau might be still needed.